mod handlers;
mod repositories;

use axum::{
    extract::Extension,
    http::HeaderValue,
    routing::{delete, get, post},
    Router,
};
use std::env;

use dotenv::dotenv;
use handlers::label::{all_label, create_label, delete_label};
use handlers::todo::{all_todo, create_todo, delete_todo, find_todo, update_todo};
use hyper::header::CONTENT_TYPE;
use repositories::label::{LabelRepository, LabelRepositoryForDb};
use repositories::todo::{TodoRepository, TodoRepositoryForDb};
use sqlx::PgPool;
use std::sync::Arc;
use tower_http::cors::{Any, CorsLayer};

#[tokio::main]
async fn main() {
    dotenv().ok();

    let database_url = &env::var("DATABASE_URL").expect("undefined [DATABASE_URL]");
    let pool = PgPool::connect(database_url).await.expect(&format!(
        "failed connect database, url is [{}]",
        database_url
    ));
    sqlx::migrate!("./migrations")
        .run(&pool)
        .await
        .expect("error migrations");

    let app = create_app(
        TodoRepositoryForDb::new(pool.clone()),
        LabelRepositoryForDb::new(pool.clone()),
    );
    let listener = tokio::net::TcpListener::bind("127.0.0.1:3012")
        .await
        .unwrap();

    println!("listening on {}", listener.local_addr().unwrap());

    axum::serve(listener, app).await.unwrap();
}

async fn root() -> &'static str {
    "Hello, World"
}

fn create_app<Todo: TodoRepository, Label: LabelRepository>(
    todo_repository: Todo,
    label_repository: Label,
) -> Router {
    Router::new()
        .route("/", get(root))
        .route("/todos", post(create_todo::<Todo>).get(all_todo::<Todo>))
        .route(
            "/todos/:id",
            get(find_todo::<Todo>)
                .delete(delete_todo::<Todo>)
                .patch(update_todo::<Todo>),
        )
        .route(
            "/labels",
            post(create_label::<Label>).get(all_label::<Label>),
        )
        .route("/labels/:id", delete(delete_label::<Label>))
        .layer(Extension(Arc::new(todo_repository)))
        .layer(Extension(Arc::new(label_repository)))
        .layer(
            CorsLayer::new()
                .allow_origin("http://localhost:5173".parse::<HeaderValue>().unwrap())
                .allow_methods(Any)
                .allow_headers(vec![CONTENT_TYPE]),
        )
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::repositories::label::{test_utils::LabelRepositoryForMemory, CreateLabel, Label};
    use crate::repositories::todo::{test_utils::TodoRepositoryForMemory, CreateTodo, TodoEntity};
    use axum::{
        body::Body,
        http::{header, Method, Request, StatusCode},
        response::Response,
    };
    use http_body_util::BodyExt;
    use serde::Deserialize;
    use tower::ServiceExt;

    fn build_todo_req_with_json(path: &str, method: Method, json_body: String) -> Request<Body> {
        Request::builder()
            .uri(path)
            .method(method)
            .header(header::CONTENT_TYPE, mime::APPLICATION_JSON.as_ref())
            .body(Body::from(json_body))
            .unwrap()
    }

    fn build_todo_req_with_empty(method: Method, path: &str) -> Request<Body> {
        Request::builder()
            .uri(path)
            .method(method)
            .body(Body::empty())
            .unwrap()
    }

    fn build_app_for_todo<Todo: TodoRepository>(todo_repository: Todo) -> Router {
        create_app(todo_repository, LabelRepositoryForMemory::new())
    }

    fn build_app_for_label<Label: LabelRepository>(label_repository: Label) -> Router {
        create_app(TodoRepositoryForMemory::new(), label_repository)
    }

    async fn res_to<T>(res: Response) -> T
    where
        T: for<'de> Deserialize<'de>,
    {
        let bytes = res.into_body().collect().await.unwrap().to_bytes();
        let body = String::from_utf8(bytes.to_vec()).unwrap();
        let res: T = serde_json::from_str(&body)
            .expect(&format!("cannot convert Todo instace. body: {}", body));
        res
    }

    #[tokio::test]
    async fn should_created_label() {
        let expected = Label::new(1, "should_return_created_label".to_string());

        let repository = LabelRepositoryForMemory::new();
        let req = build_todo_req_with_json(
            "/labels",
            Method::POST,
            r#"{"name": "should_return_created_label"}"#.to_string(),
        );
        let res = build_app_for_label(repository).oneshot(req).await.unwrap();
        let label: Label = res_to(res).await;
        assert_eq!(label, expected);
    }

    #[tokio::test]
    async fn should_get_all_labels() {
        let expected = Label::new(1, "should_get_all_labels".to_string());

        let repository = LabelRepositoryForMemory::new();
        repository
            .create(CreateLabel::new("should_get_all_labels".to_string()))
            .await
            .expect("failed create label");
        let req = build_todo_req_with_empty(Method::GET, "/labels");
        let res = build_app_for_label(repository).oneshot(req).await.unwrap();
        let labels: Vec<Label> = res_to(res).await;
        assert_eq!(labels, vec![expected]);
    }

    #[tokio::test]
    async fn should_delete_label() {
        let repository = LabelRepositoryForMemory::new();
        repository
            .create(CreateLabel::new("should_delete_label".to_string()))
            .await
            .expect("failed create label");
        let req = build_todo_req_with_empty(Method::DELETE, "/labels/1");
        let res = build_app_for_label(repository).oneshot(req).await.unwrap();

        assert_eq!(res.status(), StatusCode::NO_CONTENT);
    }

    #[tokio::test]
    async fn should_created_todo() {
        let expected = TodoEntity::new(1, "should_return_created_todo".to_string());

        let repository = TodoRepositoryForMemory::new();
        let req = build_todo_req_with_json(
            "/todos",
            Method::POST,
            r#"{"text": "should_return_created_todo"}"#.to_string(),
        );
        let res = build_app_for_todo(repository).oneshot(req).await.unwrap();
        let todo: TodoEntity = res_to(res).await;
        assert_eq!(todo, expected);
    }

    #[tokio::test]
    async fn should_find_todo() {
        let expected = TodoEntity::new(1, "should_find_todo".to_string());

        todo!("add label data");
        let labels = vec![];
        let repository = TodoRepositoryForMemory::new();
        repository
            .create(CreateTodo::new("should_find_todo".to_string(), labels))
            .await
            .expect("failed create todo");
        let req = build_todo_req_with_empty(Method::GET, "/todos/1");
        let res = build_app_for_todo(repository).oneshot(req).await.unwrap();
        let todo: TodoEntity = res_to(res).await;
        assert_eq!(todo, expected);
    }

    #[tokio::test]
    async fn should_get_all_todos() {
        let expected = TodoEntity::new(1, "should_get_all_todos".to_string());
        todo!("add label data");
        let labels = vec![];
        let repository = TodoRepositoryForMemory::new();
        repository
            .create(CreateTodo::new("should_get_all_todos".to_string(), labels))
            .await
            .expect("failed create todo");
        let req = build_todo_req_with_empty(Method::GET, "/todos");
        let res = build_app_for_todo(repository).oneshot(req).await.unwrap();
        let bytes = res.into_body().collect().await.unwrap().to_bytes();
        let body: String = String::from_utf8(bytes.to_vec()).unwrap();
        let todos: Vec<TodoEntity> = serde_json::from_str(&body)
            .expect(&format!("cannot convert Todo instace. body: {}", body));
        assert_eq!(todos, vec![expected]);
    }

    #[tokio::test]
    async fn should_update_todo() {
        let expected = TodoEntity::new(1, "should_update_todo".to_string());

        todo!("add label data");
        let labels = vec![];
        let repository = TodoRepositoryForMemory::new();
        repository
            .create(CreateTodo::new("before_update_todo".to_string(), labels))
            .await
            .expect("failed create todo");

        let req = build_todo_req_with_json(
            "/todos/1",
            Method::PATCH,
            r#"{
        "id": 1, "text": "should_update_todo", "completed": false
        }"#
            .to_string(),
        );
        let res = build_app_for_todo(repository).oneshot(req).await.unwrap();
        let todo: TodoEntity = res_to(res).await;
        assert_eq!(todo, expected);
    }

    #[tokio::test]
    async fn should_delete_todo() {
        todo!("add label data");
        let labels = vec![];
        let repository = TodoRepositoryForMemory::new();
        repository
            .create(CreateTodo::new("should_delete_todo".to_string(), labels))
            .await
            .expect("failed create todo");
        let req = build_todo_req_with_empty(Method::DELETE, "/todos/1");
        let res = build_app_for_todo(repository).oneshot(req).await.unwrap();

        assert_eq!(res.status(), StatusCode::NO_CONTENT);
    }

    #[tokio::test]
    async fn should_return_hello_world() {
        let repository = TodoRepositoryForMemory::new();
        let req = Request::builder().uri("/").body(Body::empty()).unwrap();
        let res = build_app_for_todo(repository).oneshot(req).await.unwrap();

        assert_eq!(res.status(), StatusCode::OK);

        let bytes = res.into_body().collect().await.unwrap().to_bytes();
        let body = String::from_utf8(bytes.to_vec()).unwrap();
        assert_eq!(&body, "Hello, World");
    }
}
