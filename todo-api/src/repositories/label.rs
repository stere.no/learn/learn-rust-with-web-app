use axum::async_trait;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use validator::Validate;

use super::RepositoryError;

#[async_trait]
pub trait LabelRepository: Clone + std::marker::Sync + std::marker::Send + 'static {
    async fn create(&self, payload: CreateLabel) -> anyhow::Result<Label>;
    async fn all(&self) -> anyhow::Result<Vec<Label>>;
    async fn delete(&self, id: i32) -> anyhow::Result<()>;
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq, sqlx::FromRow)]
pub struct Label {
    pub id: i32,
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq, Validate)]
pub struct CreateLabel {
    #[validate(length(min = 1, message = "Cannot be empty"))]
    #[validate(length(max = 100, message = "Over text length"))]
    pub name: String,
}

#[derive(Debug, Clone)]
pub struct LabelRepositoryForDb {
    pool: PgPool,
}

impl LabelRepositoryForDb {
    pub fn new(pool: PgPool) -> Self {
        Self { pool }
    }
}

#[async_trait]
impl LabelRepository for LabelRepositoryForDb {
    async fn create(&self, payload: CreateLabel) -> anyhow::Result<Label> {
        let optional_label: Option<Label> = sqlx::query_as(
            r#"
                select * from labels
                where name = $1
            "#,
        )
        .bind(payload.name.clone())
        .fetch_optional(&self.pool)
        .await?;

        if let Some(label) = optional_label {
            return Err(RepositoryError::Duplicate(label.id).into());
        }

        let label = sqlx::query_as(
            r#"
                insert into labels (name)
                values ($1)
                returning *
            "#,
        )
        .bind(payload.name.clone())
        .fetch_one(&self.pool)
        .await?;

        Ok(label)
    }
    async fn all(&self) -> anyhow::Result<Vec<Label>> {
        let labels = sqlx::query_as(
            r#"
                select * from labels
                order by labels.id asc;
            "#,
        )
        .fetch_all(&self.pool)
        .await?;

        Ok(labels)
    }
    async fn delete(&self, id: i32) -> anyhow::Result<()> {
        sqlx::query(
            r#"
                delete from labels
                where id = $1
            "#,
        )
        .bind(id)
        .execute(&self.pool)
        .await
        .map_err(|e| match e {
            sqlx::Error::RowNotFound => RepositoryError::NotFound(id),
            _ => RepositoryError::Unexpected(e.to_string()),
        })?;

        Ok(())
    }
}

#[cfg(test)]
#[cfg(feature = "database-test")]
mod test {
    use super::*;
    use dotenv::dotenv;
    use sqlx::PgPool;
    use std::env;

    #[tokio::test]
    async fn crud_scenario() {
        dotenv().ok();
        let database_url = &env::var("DATABASE_URL").expect("undefined [DATABASE_URL]");
        let pool = PgPool::connect(database_url)
            .await
            .expect(&format!("fail connect database, url is [{}]", database_url));

        let repository = LabelRepositoryForDb::new(pool);

        let label_text = CreateLabel {
            name: "test_label".to_string(),
        };

        let label = repository
            .create(label_text.clone())
            .await
            .expect("[create] return Err");
        assert_eq!(label.name, label_text.name);

        let labels = repository.all().await.expect("[all] returned Err");
        let label = labels.last().unwrap();
        assert_eq!(label.name, label_text.name);

        let res = repository
            .delete(label.id)
            .await
            .expect("[delete] returned Err");
        assert_eq!(res, ());
    }
}

#[cfg(test)]
pub mod test_utils {

    use super::*;
    use std::collections::HashMap;
    use std::sync::{Arc, RwLock};

    impl CreateLabel {
        pub fn new(name: String) -> Self {
            Self { name }
        }
    }

    impl Label {
        pub fn new(id: i32, name: String) -> Self {
            Self {
                id,
                name: name.to_string(),
            }
        }
    }

    #[derive(Debug, Clone)]
    pub struct LabelRepositoryForMemory {
        store: Arc<RwLock<HashMap<i32, Label>>>,
    }

    impl LabelRepositoryForMemory {
        pub fn new() -> Self {
            Self {
                store: Arc::new(RwLock::new(HashMap::new())),
            }
        }
    }

    #[async_trait]
    impl LabelRepository for LabelRepositoryForMemory {
        async fn create(&self, payload: CreateLabel) -> anyhow::Result<Label> {
            let mut store = self.store.write().unwrap();
            let id = store.len() as i32 + 1;
            let label = Label::new(id, payload.name.clone());
            store.insert(id, label.clone());
            Ok(label)
        }

        async fn all(&self) -> anyhow::Result<Vec<Label>> {
            let store = self.store.read().unwrap();
            Ok(store.values().cloned().collect())
        }

        async fn delete(&self, id: i32) -> anyhow::Result<()> {
            let mut store = self.store.write().unwrap();
            store.remove(&id);
            Ok(())
        }
    }

    #[cfg(test)]
    mod test {
        use super::*;

        #[tokio::test]
        async fn label_curd_scenario() {
            let repository = LabelRepositoryForMemory {
                store: Arc::new(RwLock::new(HashMap::new())),
            };

            let label_text = CreateLabel {
                name: "test_label".to_string(),
            };

            let label = repository
                .create(label_text.clone())
                .await
                .expect("[create] return Err");
            assert_eq!(label.name, label_text.name);

            let labels = repository.all().await.expect("[all] returned Err");
            let label = labels.last().unwrap();
            assert_eq!(label.name, label_text.name);

            let res = repository
                .delete(label.id)
                .await
                .expect("[delete] returned Err");
            assert_eq!(res, ());
        }
    }
}
