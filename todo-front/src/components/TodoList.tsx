import { Todo } from "../types/todo";
import { Stack, Typography } from "@mui/material";
import { TodoItem } from "./TodoItem";

type Props = {
	todos: Todo[];
	onUpdate: (todo: Todo) => void;
	onDelete: (id: number) => void;
};

export function TodoList({ todos, onUpdate, onDelete }: Props) {
	const handleCompletedCheckbox = (todo: Todo) => {
		onUpdate({
			...todo,
			completed: !todo.completed,
		});
	};

	return (
		<>
			<Stack spacing={2}>
				<Typography variant="h2">todo list</Typography>
				<Stack spacing={2}>
					{todos.map((todo) => (
						<TodoItem todo={todo} onUpdate={onUpdate} onDelete={onDelete} />
					))}
				</Stack>
			</Stack>
		</>
	);
}
