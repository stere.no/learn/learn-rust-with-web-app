import { useState } from "react";
import { NewTodoPayload } from "../types/todo";
import { Box, Button, TextField, Paper, Grid } from "@mui/material";

type Props = {
	onSubmit: (newTodo: NewTodoPayload) => void;
};

export function TodoForm({ onSubmit }: Props) {
	const [editText, setEditText] = useState("");

	const addTodoHandler = async () => {
		if (!editText) return;

		onSubmit({
			text: editText,
		});
		setEditText("");
	};
	return (
		<>
			<Paper elevation={2}>
				<Box
					sx={{
						p: 2,
					}}
				>
					<Grid container rowSpacing={2} columnSpacing={5}>
						<Grid item xs={12}>
							<TextField
								label="new todo text"
								variant="filled"
								onChange={(e) => setEditText(e.target.value)}
								fullWidth
							/>
						</Grid>
						<Grid item xs={9} />
						<Grid item xs={3}>
							<Button onClick={addTodoHandler} fullWidth>
								add todo
							</Button>
						</Grid>
					</Grid>
				</Box>
			</Paper>
		</>
	);
}
