import { ChangeEventHandler, useState, useEffect } from "react";
import {
	Card,
	Modal,
	Box,
	Stack,
	Grid,
	Checkbox,
	Typography,
	Button,
	TextField,
} from "@mui/material";
import { Todo } from "../types/todo";
import { modalInnerStyle } from "../styles/modal";

type Props = {
	todo: Todo;
	onUpdate: (todo: Todo) => void;
	onDelete: (id: number) => void;
};

export function TodoItem({ todo, onUpdate, onDelete }: Props) {
	const [editing, setEditing] = useState(false);
	const [editText, setEditText] = useState("");

	useEffect(() => {
		setEditText(todo.text);
	}, [todo]);

	const onCloseEditModal = () => {
		onUpdate({
			...todo,
			text: editText,
		});
		setEditing(false);
	};

	const handleCompletedCheckbox = (todo: Todo) => {
		onUpdate({
			...todo,
			completed: !todo.completed,
		});
	};

	const handleDelete = () => onDelete(todo.id);

	return (
		<>
			<Card key={todo.id} sx={{ p: 2 }}>
				<Grid container spacing={2} alignItems={"center"}>
					<Grid item xs={1}>
						<Checkbox
							checked={todo.completed}
							onChange={() => handleCompletedCheckbox(todo)}
						/>
					</Grid>
					<Grid item xs={8}>
						<Typography variant="body1">{todo.text}</Typography>
					</Grid>
					<Grid item xs={1}>
						<Button onClick={() => setEditing(true)} color="info">
							Edit
						</Button>
					</Grid>
					<Grid item xs={1}>
						<Button onClick={handleDelete} color="error">
							delete
						</Button>
					</Grid>
				</Grid>
				<Modal open={editing} onClose={onCloseEditModal}>
					<Box sx={modalInnerStyle}>
						<Stack spacing={2}>
							<TextField
								size="small"
								label="todo text"
								defaultValue={todo.text}
								onChange={(e) => setEditText(e.target.value)}
							/>
						</Stack>
					</Box>
				</Modal>
			</Card>
		</>
	);
}
