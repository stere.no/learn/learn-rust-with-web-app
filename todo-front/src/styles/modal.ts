export const modalInnerStyle = {
	position: "absolute" as "absolute",
	top: "50%",
	left: "50%",
	transform: "translate(-50%, -50%)",
	width: 400,
	backgroundColor: "background.paper",
	boxShadow: 24,
	padding: 4,
};
