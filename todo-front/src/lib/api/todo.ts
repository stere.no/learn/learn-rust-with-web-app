import type { NewTodoPayload, Todo } from "../../types/todo";

export async function addTodoItem(payload: NewTodoPayload) {
	const res = await fetch("http://localhost:3012/todos", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify(payload),
	});
	if (!res.ok) {
		throw new Error("add todo request failed");
	}
	const json: Todo = await res.json();
	return json;
}

export async function getTodoItems() {
	const res = await fetch("http://localhost:3012/todos");
	if (!res.ok) {
		throw new Error("get todo request failed");
	}
	const json: Todo[] = await res.json();
	return json;
}

export async function updateTodoItem(todo: Todo) {
	const { id, ...updateTodo } = todo;
	const res = await fetch(`http://localhost:3012/todos/${id}`, {
		method: "PATCH",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify(updateTodo),
	});
	if (!res.ok) {
		throw new Error("upate todo request failed");
	}
	const json: Todo = await res.json();
	return json;
}

export async function deleteTodoItem(id: number) {
	const res = await fetch(`http://localhost:3012/todos/${id}`, {
		method: "DELETE",
	});
	if (!res.ok) {
		throw new Error("delete todo request failed");
	}
}
